import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.elementAt
import strikt.assertions.endsWith
import strikt.assertions.hasSize
import strikt.assertions.isFalse
import strikt.assertions.isTrue
import strikt.assertions.startsWith

class PassportProcessingTest {
    @Nested
    inner class PassportTest {
        private val fullPassportProperties = mapOf(
            "byr" to "1937",
            "iyr" to "2017",
            "eyr" to "2020",
            "hgt" to "183cm",
            "hcl" to "#fffffd",
            "ecl" to "gry",
            "pid" to "860033327",
            "cid" to "147"
        )

        @Test
        fun `passport with all properties is valid`() {
            val passport = Passport(fullPassportProperties)
            expectThat(passport).get(Passport::isValid).isTrue()
        }

        @Test
        fun `passport without byr is invalid`() {
            val passport = Passport(fullPassportProperties - "byr")
            expectThat(passport).get(Passport::isValid).isFalse()
        }

        @Test
        fun `passport without cid is valid`() {
            val passport = Passport(fullPassportProperties - "cid")
            expectThat(passport).get(Passport::isValid).isTrue()
        }

        @Test
        fun `passport with byr out of range is invalid`() {
            val passport = Passport(fullPassportProperties + ("byr" to "1919"))
            expectThat(passport).get(Passport::isValid).isFalse()
        }

        @Test
        fun `passport with byr not a year is invalid`() {
            val passport = Passport(fullPassportProperties + ("byr" to "1930v"))
            expectThat(passport).get(Passport::isValid).isFalse()
        }

        @Test
        fun `passport with cm height out of range is invalid`() {
            val passport = Passport(fullPassportProperties + ("hgt" to "300cm"))
            expectThat(passport).get(Passport::isValid).isFalse()
        }

        @Test
        fun `passport with inches height out of range is invalid`() {
            val passport = Passport(fullPassportProperties + ("hgt" to "2in"))
            expectThat(passport).get(Passport::isValid).isFalse()
        }

        @Test
        fun `passport with invalid hair colour is invalid`() {
            val passport = Passport(fullPassportProperties + ("hcl" to "123abc"))
            expectThat(passport).get(Passport::isValid).isFalse()
        }

        @Test
        fun `passport with invalid eye colour is invalid`() {
            val passport = Passport(fullPassportProperties + ("ecl" to "2in"))
            expectThat(passport).get(Passport::isValid).isFalse()
        }

        @Test
        fun `passport with invalid pid is invalid`() {
            val passport = Passport(fullPassportProperties + ("pid" to "0123456789"))
            expectThat(passport).get(Passport::isValid).isFalse()
        }
    }

    @Nested
    inner class PassportFileParserTest {
        val exampleInput = """
            ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
            byr:1937 iyr:2017 cid:147 hgt:183cm

            iyr:2013 ecl:amb cid:350 eyr:2023 pid:028048884
            hcl:#cfa07d byr:1929

            hcl:#ae17e1 iyr:2013
            eyr:2024
            ecl:brn pid:760753108 byr:1931
            hgt:179cm

            hcl:#cfa07d eyr:2025 pid:166559648
            iyr:2011 ecl:brn hgt:59in
        """.trimIndent()

        @Test
        fun `should split file input by blank lines`() {
            val fileSections: List<String> = splitByBlankLines(exampleInput)
            expectThat(fileSections)
                .hasSize(4)
                .elementAt(2).startsWith("hcl:#ae17e1").endsWith("hgt:179cm")
        }

        @Test
        fun `should parse a single passport text block`() {
            val passportText = """
                ecl:gry pid:860033327 eyr:2020 hcl:#fffffd
                byr:1937 iyr:2017 cid:147 hgt:183cm
            """.trimIndent()

            val passport = parsePassport(passportText)
            expectThat(passport).get(Passport::isValid).isTrue()
        }
    }
}
