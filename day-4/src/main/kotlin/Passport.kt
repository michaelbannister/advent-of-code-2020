import kotlin.text.RegexOption.MULTILINE

fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val passports = splitByBlankLines(input).map(::parsePassport)
    val validCount = passports.count(Passport::isValid)
    println("$validCount valid passports in file")
}

class Passport(private val fields: Map<String, String>) {
    val validators = mapOf(
        "byr" to yearValidator("1920", "2002"),
        "iyr" to yearValidator("2010", "2020"),
        "eyr" to yearValidator("2020", "2030"),
        "hgt" to heightValidator,
        "hcl" to hairColourValidator,
        "ecl" to eyeColourValidator,
        "pid" to passportNubmerValidator
    ).withDefault { assumeValid }
    val requiredFields = setOf("byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid")
    fun isValid(): Boolean = fields.keys.containsAll(requiredFields) &&
            fields.all { (k, v) -> validators.getValue(k)(v) }
}
typealias Validator = (String) -> Boolean


private val assumeValid: Validator = { _ -> true}
private fun yearValidator(low: String, high: String): Validator = { value ->
    value.length == 4 && value.all(Char::isDigit) && value in low..high
}
private val heightValidator: Validator = { value ->
    Regex("(\\d{2,3})(cm|in)").matchEntire(value)?.let { match ->
        val (number, unit) = match.destructured
        when (unit) {
            "cm" -> number.toInt() in 150..193
            "in" -> number.toInt() in 59..76
            else -> false
        }
    } ?: false
}
private val hairColourValidator: Validator = { value ->
    value matches Regex("#[0-9a-f]{6}")
}
private val eyeColourValidator: Validator = { value ->
    value in setOf("amb", "blu", "brn", "gry", "grn", "hzl", "oth")
}
private val passportNubmerValidator: Validator = { value ->
    value.length == 9 && value.all(Char::isDigit)
}

fun parsePassport(text: String): Passport =
    text.split(Regex("\\s+", MULTILINE))
        .map {
            val (k, v) = it.split(":")
            k to v
        }
        .toMap()
        .let(::Passport)

fun splitByBlankLines(text: String): List<String> = text.split("\n\n")