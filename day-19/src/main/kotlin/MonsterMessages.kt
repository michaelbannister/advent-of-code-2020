fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val (rulesInput, messagesInput) = input.split("\n\n")
    val rules = Rules(rulesInput)
    val validCount = messagesInput.lines().count(rules::isMessageValid)
    println(validCount)

    val input2 = object {}::class.java.getResource("input2.txt")!!.readText()
    val newRules = Rules(input2.split("\n\n").first(), version = 2)
    val validCount2 = messagesInput.lines().count(newRules::isMessageValid)
    println(validCount2)
    println(newRules.matcher.pattern)
}

class Rules(input: String, version: Int = 1) {
    val rules: Map<String, Rule>
    val matcher: Regex
    val characterRule = Regex("\"\\w\"")

    init {
        val ruleSplitter = Regex("""(\d+): (.*)""")
        rules = input.lines().map {
            val (number, value) = ruleSplitter.find(it)?.destructured ?: error("invalid input")
            number to Rule(number, value, version)
        }.toMap()

        val zero = rules.getValue("0")
        matcher = Regex("^${zero.pattern}$")
    }

    inner class Rule(private val number: String, private val text: String, version: Int) {
        val pattern: String by lazy {
            if (characterRule.matches(text)) {
                text.removePrefix("\"").removeSuffix("\"")
            } else if (version == 2 && number == "8") {
                rules.getValue("42").pattern + "+"
            } else if (version == 2 && number == "11") {
                val p42 = rules.getValue("42").pattern
                val p31 = rules.getValue("31").pattern
                (1..44).joinToString("|") { repeat ->
                    "$p42{$repeat}$p31{$repeat}".wrapWithNonCapturingGroup()
                }.wrapWithNonCapturingGroup()
            } else {
                text.split(' ').joinToString("") {
                    when (it) {
                        "|" -> "|"
                        else -> rules.getValue(it).pattern
                    }
                }.wrapWithNonCapturingGroup()
            }
        }
    }

    fun isMessageValid(message: String) = matcher.matches(message)
}

fun String.wrapWithNonCapturingGroup() = "(?:$this)"