import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

class MonsterMessagesTest {
    val exampleRules = """
        0: 4 1 5
        1: 2 3 | 3 2
        2: 4 4 | 5 5
        3: 4 5 | 5 4
        4: "a"
        5: "b"
    """.trimIndent()

    @Test
    fun `simple character rule`() {
        val rules = Rules(exampleRules)

        val messages = """
            ababbb
            bababa
            abbbab
            aaabbb
            aaaabbb
        """.trimIndent().lines()
        expectThat(messages.count { it.matches(rules.matcher) }) isEqualTo 2
    }
}