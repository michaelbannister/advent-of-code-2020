import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

class AdapterArrayTest {
    @Test
    fun `count arrangements for sample input`() {
        val sampleInput = """
            16
            10
            15
            5
            1
            11
            7
            19
            6
            12
            4
        """.trimIndent()
        val combinations = calculateArrangements(parseAdaptersInput(sampleInput))
        expectThat(combinations) isEqualTo 8
    }

    @Test
    fun `count arrangements for second sample input`() {
        val sampleInput = """
            28
            33
            18
            42
            31
            14
            46
            20
            48
            47
            24
            23
            49
            45
            19
            38
            39
            11
            1
            32
            25
            35
            8
            17
            7
            9
            4
            2
            34
            10
            3
        """.trimIndent()
        val combinations = calculateArrangements(parseAdaptersInput(sampleInput))
        expectThat(combinations) isEqualTo 19208
    }

    @Test
    fun `count arrangements for my worked example`() {
        val input = """
            1
            2
            3
            5
            6
            7
            10
            11
            12
        """.trimIndent()
        val combinations = calculateArrangements(parseAdaptersInput(input))
        expectThat(combinations) isEqualTo 32
    }
}