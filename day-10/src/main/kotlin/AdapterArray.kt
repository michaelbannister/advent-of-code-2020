fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val adapters = parseAdaptersInput(input)
    val differences = calculateDifferences(adapters)

    val answer = (differences.count { it == 1L }) * (differences.count { it == 3L })
    println(answer)

    val arrangements = calculateArrangements(adapters)
    println(arrangements)
}

fun parseAdaptersInput(input: String) =
    input.lines().map(String::toLong).sorted().withInitialZero().withBuiltInAdapter()

// adapters list must be sorted
fun calculateDifferences(adapters: List<Long>) =
    adapters.windowed(2) { it[1] - it[0] }

fun calculateArrangements(adapters: List<Long>): Long {
    val differences = calculateDifferences(adapters)
    val workingCounts = mutableListOf<Long>(0, 1, 1)

    differences.asReversed().windowed(3).forEach { diffWindow ->
        if (diffWindow.size == 3 && diffWindow == listOf<Long>(1, 1, 1)) {
            val next = workingCounts.takeLast(3).sum()
            workingCounts.add(next)
        } else if (diffWindow.takeLast(2).contains(3)) {
            workingCounts.add(workingCounts.last())
        } else {
            val next = workingCounts.takeLast(2).sum()
            workingCounts.add(next)
        }
    }
    return workingCounts.last()
}

fun List<Long>.withInitialZero() = listOf(0L) + this
fun List<Long>.withBuiltInAdapter() = this + (this.last() + 3)