rootProject.name = "advent-of-code-2020"
val days = (1..19).map {"day-$it"}.toTypedArray()
include(*days)
