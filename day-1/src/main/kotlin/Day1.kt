import java.io.File
import java.math.BigInteger
import java.time.Duration
import java.time.Instant
import java.util.*
import kotlin.system.exitProcess

val TARGET = 2020.toBigInteger()

fun main() {

    val file = File("src/main/resources/input.txt")
    val numbers = file.readLines().map(String::toBigInteger)

    printElapsedTime {
        printSolution(solve2(numbers))
    }
    printElapsedTime {
        printSolution(solve3(numbers))
    }
}

internal fun solve2(numbers: Iterable<BigInteger>): Set<BigInteger> {
    numbers.forEach { a ->
        numbers.minus(a).forEach { b ->
            if (a + b == TARGET) {
                return setOf(a, b)
            }
        }
    }
    return emptySet()
}

internal fun solve3(numbers: Iterable<BigInteger>): Set<BigInteger> {
    numbers.forEach { a ->
        numbers.minus(a).forEach { b ->
            numbers.minus(a).minus(b).forEach { c ->
                if (a + b + c == TARGET) {
                    return setOf(a, b, c)
                }
            }
        }
    }
    return emptySet()
}


internal fun printSolution(solution: Collection<BigInteger>) {
    if (solution.isNotEmpty()) {
        println(solution.joinToString(" * ") + " = " + solution.reduce(BigInteger::times))
    } else {
        println("FAILED")
        exitProcess(1)
    }
}

private inline fun printElapsedTime(block: () -> Unit) {
    val start = Instant.now()
    block()
    val end = Instant.now()
    println("took " + Duration.between(start, end))
}
