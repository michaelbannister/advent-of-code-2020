import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.Arguments.arguments
import org.junit.jupiter.params.provider.MethodSource
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import java.util.stream.Stream

@Suppress("unused")
class OperationOrderTest {

    @ParameterizedTest
    @MethodSource("testExpressionsV1")
    fun `evaluate with v1 rules`(expression: String, answer: Long, description: String) {
        expectThat(evaluate(expression, Reductions.v1)).describedAs(description) isEqualTo answer
    }

    @ParameterizedTest
    @MethodSource("testExpressionsV2")
    fun `evaluate with v2 rules`(expression: String, answer: Long, description: String) {
        expectThat(evaluate(expression, Reductions.v2)).describedAs(description) isEqualTo answer
    }

    companion object {
        @JvmStatic
        fun testExpressionsV1(): Stream<Arguments> {
            return Stream.of(
                arguments("3 + 3", 6, "simple sum"),
                arguments("3 * 3", 9, "simple product"),
                arguments("3 * (2 + 1)", 9, "simple parentheses on right hand side"),
                arguments("3 + 1 * 2", 8, "two consecutive operations"),
                arguments("2 * 3 + (4 * 5)", 26, "further example"),
                arguments("5 + (8 * 3 + 9 + 3 * 4 * 3)", 437, "further example"),
                arguments("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 12240, "further example"),
                arguments("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 13632, "expression starting with (redundant) parentheses")
            )
        }

        @JvmStatic
        fun testExpressionsV2(): Stream<Arguments> {
            return Stream.of(
                arguments("3 + 3", 6, "simple sum"),
                arguments("3 * 3", 9, "simple product"),
                arguments("3 + 1 * 2", 8, "two consecutive operations"),
                arguments("3 * 2 + 1", 9, "addition before multiplication"),
                arguments("(2 * 3) + (4 * 5)", 26, "parenthesis for multiplication to take precedence"),
                arguments("5 + (8 * 3 + 9 + 3 * 4 * 3)", 1445, "further example"),
                arguments("5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 669060, "further example"),
                arguments("((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 23340, "expression starting with (redundant) parentheses")
            )
        }
    }
}