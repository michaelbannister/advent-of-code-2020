fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val sum = input.lines().fold(0L) { sum, expression ->
        sum + evaluate(expression, Reductions.v1)
    }
    println(sum)

    val sum2 = input.lines().fold(0L) { sum, expression ->
        sum + evaluate(expression, Reductions.v2)
    }
    println(sum2)
}


val done = Regex("""^\d+$""")
val innermostParentheses = Regex("""\(([^()]+)\)""")

tailrec fun evaluate(expression: String, reductions: Sequence<Reduction>): Long {
    return when {
        expression.matches(done) -> {
            expression.toLong()
        }
        expression.contains('(') -> {
            val match = innermostParentheses.find(expression) ?: error("WTF: $expression")
            val reduction = evaluate(match.groupValues[1], reductions).toString()
            val reducedExpression = expression.replaceRange(match.range, reduction)
            evaluate(reducedExpression, reductions)
        }
        else -> {
            val reduced = reductions.mapNotNull { it.tryApply(expression) }.first()
            evaluate(reduced, reductions)
        }
    }
}

data class Reduction(val regex: Regex, val replacement: (MatchResult) -> String) {
    fun tryApply(expression: String): String? = regex.find(expression)?.let { matchResult ->
        val replacement = replacement(matchResult)
        expression.replaceRange(matchResult.range, replacement)
    }
}

object Reductions {
    val unnecessaryParens = Reduction(Regex("""\((\d+)\)""")) { matchResult ->
        matchResult.groupValues[1]
    }

    val multiplicationOrAddition = Reduction(Regex("""(\d+) (\*|\+) (\d+)""")) { matchResult ->
        val (left, op, right) = matchResult.destructured
        val operation: (Long, Long) -> Long = when (op) {
            "*" -> Long::times
            "+" -> Long::plus
            else -> error("invalid operator $op")
        }
        operation(left.toLong(), right.toLong()).toString()
    }
    val multiplication = Reduction(Regex("""(\d+) \* (\d+)""")) { matchResult ->
        val (left, right) = matchResult.destructured
        (left.toLong() * right.toLong()).toString()
    }
    val addition = Reduction(Regex("""(\d+) \+ (\d+)""")) { matchResult ->
        val (left, right) = matchResult.destructured
        (left.toLong() + right.toLong()).toString()
    }
    val v1 = sequenceOf(unnecessaryParens, multiplicationOrAddition)
    val v2 = sequenceOf(unnecessaryParens, addition, multiplication)
}

