fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val notes = parseNotes(input)
    val earliestBus = findEarliestBus(notes)
    println(earliestBus.busId * earliestBus.waitingTime)
}


class Notes(val earliestDepartureTimestamp: Int, val busesInService: List<Int>)
class BusDeparture(val busId: Int, val departureTimestamp: Int, val waitingTime: Int)

fun parseNotes(input: String): Notes {
    val (firstLine, secondLine) = input.lines()
    return Notes(firstLine.toInt(), secondLine.split(',').filterNot { it == "x" }.map(String::toInt))
}

fun findEarliestBus(notes: Notes): BusDeparture =
    notes.busesInService
        .map { busId -> findNextDeparture(busId, notes.earliestDepartureTimestamp) }
        .minByOrNull(BusDeparture::departureTimestamp)
        ?: throw IllegalStateException()

fun findNextDeparture(busId: Int, startingTimestamp: Int): BusDeparture =
    when (val minutesSinceLastDeparture = startingTimestamp % busId) {
        0 -> startingTimestamp
        else -> startingTimestamp + (busId - minutesSinceLastDeparture)
    }.let { BusDeparture(busId, it, it - startingTimestamp) }

