fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val busListInput = input.lines()[1]
    val startTimestamp = findFirstTimestampObeyingRule(busListInput)

    println(startTimestamp)
}

fun findFirstTimestampObeyingRule(busListInput: String): Long {
    val targetDepartureOffsets = busListInput.split(',').mapIndexedNotNull { offset, bus ->
        if (bus == "x") null
        else offset to bus.toLong()
    }.toMap()
    var timestamp = 0L
    var period = 1L
    targetDepartureOffsets.forEach { (offset, busId) ->
        timestamp = generateSequence(timestamp) { it + period }.first { (it + offset) % busId == 0L }
        period *= busId
    }

    return timestamp
}
