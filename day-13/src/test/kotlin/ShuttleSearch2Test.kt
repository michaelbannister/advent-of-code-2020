import org.junit.jupiter.api.Test
import strikt.api.expect
import strikt.api.expectThat
import strikt.assertions.containsExactly
import strikt.assertions.isEqualTo

class ShuttleSearch2Test {

    @Test
    fun `should find timestamp`() {
        expect {
            that(findFirstTimestampObeyingRule("17,x,13,19")) isEqualTo 3417
            that(findFirstTimestampObeyingRule("67,7,x,59,61")) isEqualTo 1261476
            that(findFirstTimestampObeyingRule("1789,37,47,1889")) isEqualTo 1202161486
        }
    }

}

