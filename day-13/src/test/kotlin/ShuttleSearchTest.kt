import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.containsExactly
import strikt.assertions.isEqualTo

class ShuttleSearchTest {

    val exampleInput = """
            939
            7,13,x,x,59,x,31,19
        """.trimIndent()

    @Test
    fun `should parse input`() {
        val notes = parseNotes(exampleInput)

        expectThat(notes) {
            get { earliestDepartureTimestamp } isEqualTo 939
            get { busesInService } containsExactly listOf(7, 13, 59, 31, 19)
        }
    }

    @Test
    fun `should find earliest bus`() {
        val notes = parseNotes(exampleInput)
        expectThat(findEarliestBus(notes)) {
            get { busId } isEqualTo 59
            get { departureTimestamp } isEqualTo 944
        }
    }
}

