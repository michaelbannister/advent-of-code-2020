import org.junit.jupiter.api.Test
import strikt.api.expect
import strikt.assertions.isEqualTo

class BinaryBoardingTest {
    @Test
    fun `parse row`() {
        expect {
            that(Seat("BFFFBBFRRR").row) isEqualTo 70
            that(Seat("FFFBBBFRRR").row) isEqualTo 14
            that(Seat("BBFFBBFRLL").row) isEqualTo 102
        }
    }

    @Test
    fun `parse column`() {
        expect {
            that(Seat("BFFFBBFRRR").column) isEqualTo 7
            that(Seat("FFFBBBFRRR").column) isEqualTo 7
            that(Seat("BBFFBBFRLL").column) isEqualTo 4
        }
    }

    @Test
    fun `calculate seat ID`() {
        expect {
            that(Seat("BFFFBBFRRR").seatId) isEqualTo 567
            that(Seat("FFFBBBFRRR").seatId) isEqualTo 119
            that(Seat("BBFFBBFRLL").seatId) isEqualTo 820
        }
    }
}