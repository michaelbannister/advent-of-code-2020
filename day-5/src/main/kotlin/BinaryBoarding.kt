fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val seatIds = input.lines().map(::Seat).map(Seat::seatId)
    val highestSeatId = seatIds.maxOrNull()
    println("Highest seat ID: $highestSeatId")

    val neighboursOfMissingSeat = seatIds.sorted().windowed(2).find { (a, b) -> b - a == 2 }!!
    println("My seat ID: ${neighboursOfMissingSeat[0] + 1}")
}

class Seat(spec: String) {
    val row: Int = spec.take(7).fold(0) { value, char ->
        (value shl 1) + rowValue(char)
    }
    val column: Int = spec.takeLast(3).fold(0) { value, char ->
        (value shl 1) + columnValue(char)
    }
    val seatId = row * 8 + column

    companion object {
        private fun rowValue(c: Char) = when(c) {
            'F' -> 0
            'B' -> 1
            else -> throw IllegalArgumentException("Unexpected character '$c'")
        }
        private fun columnValue(c: Char) = when(c) {
            'L' -> 0
            'R' -> 1
            else -> throw IllegalArgumentException("Unexpected character '$c'")
        }
    }
}
