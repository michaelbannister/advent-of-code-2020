import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import strikt.api.expect
import strikt.api.expectThat
import strikt.assertions.contains
import strikt.assertions.containsExactlyInAnyOrder
import strikt.assertions.isA
import strikt.assertions.isEqualTo
import strikt.assertions.isFalse
import strikt.assertions.isTrue

class HandheldHaltingTest {
    @Nested
    inner class ParseInstruction {
        @Test
        fun `parse nop`() {
            expectThat(parseInstruction("nop +0")).isA<Nop>()
        }

        @Test
        fun `parse jmp`() {
            expect {
                that(parseInstruction("jmp +4")).isA<Jump>().get { value } isEqualTo 4
                that(parseInstruction("jmp -2")).isA<Jump>().get { value } isEqualTo -2
            }
        }

        @Test
        fun `parse acc`() {
            expect {
                that(parseInstruction("acc +12")).isA<Accumulate>().get { value } isEqualTo 12
                that(parseInstruction("acc -99")).isA<Accumulate>().get { value } isEqualTo -99
            }
        }
    }

    @Nested
    inner class RunProgram {
        val exampleProgram = """
            nop +0
            acc +1
            jmp +4
            acc +3
            jmp -3
            acc -99
            acc +1
            jmp -4
            acc +6
        """.trimIndent()

        @Test
        fun `should halt on revisiting instruction`() {
            val trivialProgram = """
                nop +0
                jmp -1
            """.trimIndent()
            expectThat(runProgram(trivialProgram)) {
                get(Result::accumulator) isEqualTo 0
                get(Result::aborted).isTrue()
            }
        }

        @Test
        fun `should accumulate`() {
            val veryNearlyTrivialProgram = """
                acc +1
                jmp -1
            """.trimIndent()
            expectThat(runProgram(veryNearlyTrivialProgram)).get(Result::accumulator) isEqualTo 1
        }

        @Test
        fun `should run example program`() {
            expectThat(runProgram(exampleProgram)) {
                get(Result::accumulator) isEqualTo 5
                get(Result::aborted).isTrue()
            }
        }

        @Test
        fun `should halt on reaching end of program`() {
            val nicelyEndingProgram = """
                acc +4
                jmp +2
                nop +0
            """.trimIndent()
            expectThat(runProgram(nicelyEndingProgram)) {
                get(Result::accumulator) isEqualTo 4
                get(Result::successful).isTrue()
            }
        }
    }

    @Nested
    inner class GeneratePossibleFixes {
        @Test
        fun `replace nop with jmp`() {
            val program = """
                nop +3
                acc -1
            """.trimIndent()

            val fixes = generatePossibleFixes(program)
            expectThat(fixes.toList())
                .contains(
                    """
                    jmp +3
                    acc -1
                """.trimIndent()
                )
        }

        @Test
        fun `replace jmp with nop`() {
            val program = """
                jmp -2
                acc -1
            """.trimIndent()

            val fixes = generatePossibleFixes(program)
            expectThat(fixes.toList())
                .contains(
                    """
                    nop -2
                    acc -1
                """.trimIndent()
                )
        }

        @Test
        fun `generates multiple fixes`() {
            val program = """
                nop +0
                acc +99
                jmp -2
            """.trimIndent()

            val fixes = generatePossibleFixes(program)
            expectThat(fixes.toList())
                .containsExactlyInAnyOrder(
                    """
                    jmp +0
                    acc +99
                    jmp -2
                    """.trimIndent(),
                    """
                    nop +0
                    acc +99
                    nop -2
                    """.trimIndent()
                )
        }
    }
}