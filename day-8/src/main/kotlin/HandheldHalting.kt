import kotlin.text.RegexOption.MULTILINE

fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val resultAtHalt = runProgram(input)

    println(resultAtHalt)

    val result = generatePossibleFixes(input).map(::runProgram).first(Result::successful)
    println(result.accumulator)
}

sealed class Instruction {
    abstract fun execute(state: State): State
}
object Nop : Instruction() {
    override fun execute(state: State) = state.run { copy(pointer = pointer + 1) }
}

class Jump(val value: Int) : Instruction() {
    override fun execute(state: State) = state.run { copy(pointer = pointer + value) }
}

class Accumulate(val value: Int) : Instruction() {
    override fun execute(state: State) = state.run { copy(pointer = pointer + 1, accumulator = accumulator + value) }
}

data class State(val pointer: Int, val accumulator: Int) {
    companion object {
        val INITIAL = State(0,0)
    }
}
data class Result(val accumulator: Int, val successful: Boolean) {
    val aborted get() = !successful
}

fun parseInstruction(instruction: String): Instruction {
    val (operator, argument) = instruction.split(' ', limit = 2)
    return when (operator) {
        "nop" -> Nop
        "jmp" -> Jump(argument.toInt())
        "acc" -> Accumulate(argument.toInt())
        else -> throw IllegalArgumentException("Unrecognised instruction $operator")
    }
}

fun runProgram(program: String): Result {
    val instructions = program.lines().map(::parseInstruction)
    return execute(instructions, State.INITIAL, emptySet())
}

tailrec fun execute(instructions: List<Instruction>, state: State, previouslyExecuted: Set<InstructionPointer>): Result {
    if (state.pointer in previouslyExecuted)
        return Result(state.accumulator, successful=false)

    if (state.pointer == instructions.size)
        return Result(state.accumulator, successful=true)

    val instruction = instructions[state.pointer]
    val nextState = instruction.execute(state)
    return execute(instructions, nextState, previouslyExecuted + state.pointer)
}

fun generatePossibleFixes(originalProgramListing: String): Sequence<String> = sequence {
    val matches = Regex("^(nop|jmp)", MULTILINE).findAll(originalProgramListing)
    matches.forEach { match ->
        val replacement = when (match.groupValues.first()) {
            "nop" -> "jmp"
            "jmp" -> "nop"
            else -> TODO()
        }
        yield(originalProgramListing.replaceRange(match.range, replacement))
    }
}

typealias InstructionPointer = Int
typealias Program = List<Instruction>