fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val layout = Layout(input)
    println("Seat layout stabilises with ${findStableState(layout).occupiedSeats} occupied seats")

    println("Part 2: Seat layout stabilises with ${findStableStateV2(layout).occupiedSeats} occupied seats")

}

fun findStableState(layout: Layout): Layout {
    val states = generateSequence(layout, Layout::next)
    return states.windowed(2).takeWhile { it[0] != it[1] }.last().last()
}

fun findStableStateV2(layout: Layout): Layout {
    val states = generateSequence(layout, Layout::nextV2)
    return states.windowed(2).takeWhile { it[0] != it[1] }.last().last()
}

data class Layout(val rows: List<List<Char>>) {
    constructor(layout: String) : this(layout.lines().map(String::toList))

    val occupiedSeats = rows.sumBy { it.count(Char::isOccupiedSeat) }
    private val rowRange = 0.until(rows.size)
    private val colRange = 0.until(rows.first().size)

    fun next(): Layout = Layout(
        rows.mapIndexed { rowIndex, row ->
            row.mapIndexed { colIndex, seat ->
                nextState(Pair(rowIndex, colIndex), seat)
            }
        })

    fun nextV2(): Layout = Layout(
        rows.mapIndexed { rowIndex, row ->
            row.mapIndexed { colIndex, seat ->
                nextStateWithSightlines(Pair(rowIndex, colIndex), seat)
            }
        })

    fun whatsAt(position: Position) = rows[position.first][position.second]

    private fun nextState(position: Position, seat: Char): Char {
        val occupiedNeighbours = neighbours(position).map(::whatsAt).count(Char::isOccupiedSeat)
        return if (seat.isEmptySeat() && occupiedNeighbours == 0) {
            '#'
        } else if (seat.isOccupiedSeat() && occupiedNeighbours >= 4) {
            'L'
        } else {
            seat
        }
    }

    private fun nextStateWithSightlines(position: Position, seat: Char): Char {
        val occupiedSightlines = sightlines(position).count(::firstSeatInSightlineIsOccupied)
        return if (seat.isEmptySeat() && occupiedSightlines == 0) {
            '#'
        } else if (seat.isOccupiedSeat() && occupiedSightlines >= 5) {
            'L'
        } else {
            seat
        }
    }

    private fun firstSeatInSightlineIsOccupied(sightline: Sightline): Boolean {
        return sightline.map(::whatsAt).firstOrNull { it != '.' }.isOccupiedSeat()
    }


    private fun sightlines(position: Position): Iterable<Sightline> =
        neighbourLocations.map { (rowDelta, colDelta) ->
            generateSequence(position) { position ->
                val (r, c) = position
                val nextRow = r + rowDelta
                val nextCol = c + colDelta
                if (nextRow in rowRange && nextCol in colRange) {
                    Pair(nextRow, nextCol)
                } else null
            }.drop(1)
        }

    private fun neighbours(position: Position): Iterable<Position> =
        neighbourLocations.mapNotNull { (rowDelta, colDelta) ->
            val (r, c) = position
            if (r + rowDelta in rowRange && c + colDelta in colRange) {
                Pair(r + rowDelta, c + colDelta)
            } else null
        }

    companion object {
        private val neighbourLocations = listOf(
            Pair(-1, -1), Pair(-1, 0), Pair(-1, 1),
            Pair(0, -1), Pair(0, 1),
            Pair(1, -1), Pair(1, 0), Pair(1, 1),
        )
    }
}

typealias Position = Pair<Int, Int>
typealias Sightline = Sequence<Position>

fun Char?.isEmptySeat() = this == 'L'
fun Char?.isOccupiedSeat() = this == '#'