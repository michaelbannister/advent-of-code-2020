import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

class SeatingSystemTest {
    @Test
    fun `empty seats with no neighbours become occupied`() {
        val initial = Layout("""
            ...
            .L.
            ...
        """.trimIndent())

        val expectedNextState = Layout("""
            ...
            .#.
            ...
        """.trimIndent())

        expectThat(initial.next()) isEqualTo expectedNextState
    }

    @Test
    fun `run example`() {
        val state0 = Layout("""
            L.LL.LL.LL
            LLLLLLL.LL
            L.L.L..L..
            LLLL.LL.LL
            L.LL.LL.LL
            L.LLLLL.LL
            ..L.L.....
            LLLLLLLLLL
            L.LLLLLL.L
            L.LLLLL.LL
        """.trimIndent())

        val state1 = Layout("""
            #.##.##.##
            #######.##
            #.#.#..#..
            ####.##.##
            #.##.##.##
            #.#####.##
            ..#.#.....
            ##########
            #.######.#
            #.#####.##
        """.trimIndent())

        val state2 = Layout("""
            #.LL.L#.##
            #LLLLLL.L#
            L.L.L..L..
            #LLL.LL.L#
            #.LL.LL.LL
            #.LLLL#.##
            ..L.L.....
            #LLLLLLLL#
            #.LLLLLL.L
            #.#LLLL.##
        """.trimIndent())

        expectThat(state0.next()) isEqualTo state1
        expectThat(state1.next()) isEqualTo state2
    }

    @Test
    fun `check that example stabilises`() {
        val state0 = Layout("""
            L.LL.LL.LL
            LLLLLLL.LL
            L.L.L..L..
            LLLL.LL.LL
            L.LL.LL.LL
            L.LLLLL.LL
            ..L.L.....
            LLLLLLLLLL
            L.LLLLLL.L
            L.LLLLL.LL
        """.trimIndent())
        val stableState = findStableState(state0)
        expectThat(stableState.occupiedSeats) isEqualTo 37
    }

    @Test
    fun `fun Part 2 example from penultimate state`() {
        val state = Layout("""
            #.L#.L#.L#
            #LLLLLL.LL
            L.L.L..#..
            ##L#.#L.L#
            L.L#.#L.L#
            #.L####.LL
            ..#.#.....
            LLL###LLL#
            #.LLLLL#.L
            #.L#LL#.L#
        """.trimIndent())
        val stableState = findStableStateV2(state)
        expectThat(stableState.occupiedSeats) isEqualTo 26
    }
}