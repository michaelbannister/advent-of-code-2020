fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()

    val notes = parseNotes(input)

    val allInvalidValues = notes.nearbyTickets.flatMap { it.invalidValues(notes.rules) }.sum()
    println(allInvalidValues)

    val validTickets = notes.nearbyTickets.filter { it.invalidValues(notes.rules).isEmpty() }.plusElement(notes.myTicket)
    val numberOfFields = notes.rules.size
    val fieldsByPosition = resolveFields(validTickets, 0.until(numberOfFields).toSet(), notes.rules)

    val values = fieldsByPosition.filterValues { it.startsWith("departure") }
        .keys.map { position -> notes.myTicket[position].toLong() }
    println("values: ${values.joinToString()}")
    val productOfDepartureValues = values.reduce(Long::times)
    println(productOfDepartureValues)
}

fun resolveFields(tickets: List<Ticket>, candidatePositions: Set<Int>, candidateFieldRules: Rules): Map<Int, FieldName> {
    return if (candidatePositions.size == 1) {
        mapOf(candidatePositions.first() to candidateFieldRules.keys.first())
    } else {
        val (position, fieldName) = candidatePositions.asSequence().mapNotNull { position ->
            val matchingFields = candidateFieldRules.filter { (_, rule) -> tickets.all { ticket -> rule.isSatisfiedBy(ticket[position]) } }.keys
            if (matchingFields.size == 1) {
                position to matchingFields.first()
            } else {
                null
            }
        }.first()
        resolveFields(tickets, candidatePositions - position, candidateFieldRules - fieldName) + (position to fieldName)
    }
}

fun parseNotes(input: String): Notes {
    val (rulesInput, myTicketInput, nearbyTicketsInput) = input.split("\n\n")
    val rules = rulesInput.lines().map(::parseSingleRule).toMap()
    val myTicket = myTicketInput.linesWithoutHeader().first().let(::parseTicket)
    val nearbyTickets = nearbyTicketsInput.linesWithoutHeader().map(::parseTicket)

    return Notes(rules, myTicket, nearbyTickets)
}

val ruleRegex = Regex("""([a-z ]+): (\d+)-(\d+) or (\d+)-(\d+)""")
fun parseSingleRule(ruleText: String): Pair<FieldName, List<ClosedRange<Int>>> =
    ruleRegex.matchEntire(ruleText)!!.groupValues.drop(1).let {
        val fieldName = it.first()
        val numbers = it.drop(1).take(4).map(String::toInt)

        fieldName to listOf(numbers[0]..numbers[1], numbers[2]..numbers[3])
    }

fun parseTicket(input: String): Ticket = input.split(',').map(String::toInt)

fun Rule.isSatisfiedBy(number: Int) = any { range -> number in range }
fun Rules.anyIsSatisfiedBy(number: Int) = any { (_, rule) -> rule.isSatisfiedBy(number) }
fun Ticket.invalidValues(rules: Rules) = filterNot { rules.anyIsSatisfiedBy(it) }

data class Notes(
    val rules: Rules,
    val myTicket: Ticket,
    val nearbyTickets: List<Ticket>,
)


typealias FieldName = String
typealias Ticket = List<Int>
typealias Rule = List<ClosedRange<Int>>
typealias Rules = Map<FieldName, Rule>

fun CharSequence.linesWithoutHeader() = this.lines().drop(1)
