import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.containsExactly
import strikt.assertions.getValue
import strikt.assertions.isEqualTo
import strikt.assertions.isNotEmpty
import strikt.assertions.last

class TicketTranslationTest {

    val exampleNotes = """
        class: 1-3 or 5-7
        row: 6-11 or 33-44
        seat: 13-40 or 45-50

        your ticket:
        7,1,14

        nearby tickets:
        7,3,47
        40,4,50
        55,2,20
        38,6,12
    """.trimIndent()

    @Test
    fun `parse input`() {
        expectThat(parseNotes(exampleNotes)) {
            get { rules }.getValue("seat").containsExactly(13..40, 45..50)
            get { myTicket }.containsExactly(7, 1, 14)
            get { nearbyTickets }.isNotEmpty().last().containsExactly(38, 6, 12)
        }
    }

    @Test
    fun `count completely invalid nearby tickets`() {
        expectThat (parseNotes(exampleNotes).countCompletelyInvalidNearbyTickets()) isEqualTo 3
    }
}