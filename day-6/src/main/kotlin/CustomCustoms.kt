fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val groups = input.splitByBlankLines()
    val counts = groups.map {
        (it.toSet() - '\n').size
    }
    println(counts.sum())

    val countsPartTwo = groups.map {
        val answers = it.lines().map(String::toSet)
        val commonAnswers = answers.reduce { acc, next -> acc.intersect(next) }
        commonAnswers.size
    }
    println(countsPartTwo.sum())
}

fun String.splitByBlankLines(): List<String> = split("\n\n")