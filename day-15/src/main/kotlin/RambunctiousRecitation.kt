fun main() {
    val input = "0,13,16,17,1,10,6"
    val startingNumbers = input.split(',').map(String::toInt)
    println(speakNumbers(startingNumbers).elementAt(2020 - 1))
    println(speakNumbers(startingNumbers).elementAt(30000000 - 1))
}

fun speakNumbers(startingNumbers: List<Int>): Sequence<Int> {
    val timesSpoken = mutableMapOf<Int, MutableList<Int>>().withDefault { mutableListOf() }
    startingNumbers.forEachIndexed { index, num ->
        timesSpoken[num] = mutableListOf(index)
    }

    return sequence {
        yieldAll(startingNumbers)
        var lastNumberSpoken = startingNumbers.last()
        var index = startingNumbers.size
        while (true) {
            val previousUtterances = timesSpoken.getValue(lastNumberSpoken)
            val nextNumber = if (previousUtterances.size == 1) {
                0
            } else {
                previousUtterances.takeLast(2).let { (a, b) -> b - a }
            }
            yield(nextNumber)
            lastNumberSpoken = nextNumber
            timesSpoken[nextNumber] = timesSpoken.getValue(nextNumber).apply { add(index) }
            index++
        }
    }
}
