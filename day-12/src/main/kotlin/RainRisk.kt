import kotlin.math.absoluteValue

fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val ship = Ship(0, 0, 'E')
    input.lines().forEach(ship::move)
    println(ship.manhattanDistance)

    // Part 2
    val ship2 = Ship2()
    input.lines().forEach(ship2::performInstruction)
    println(ship2.manhattanDistance)
}

class Ship(var east: Int, var north: Int, var facing: Char) {
    fun move(instruction: String) {
        val action = instruction.first()
        val amount = instruction.drop(1).toInt()
        when (action) {
            'N' -> north += amount
            'E' -> east += amount
            'S' -> north -= amount
            'W' -> east -= amount
            'F' -> move("$facing$amount")
            'R' -> turn(amount)
            'L' -> turn(360 - amount)
        }
    }

    private fun turn(degrees: Int) {
        facing = facings[facings.indexOf(facing) + (degrees / 90)]
    }

    val position: Pair<Int, Int>
        get() = Pair(east, north)

    val manhattanDistance: Int
        get() = east.absoluteValue + north.absoluteValue

    companion object {
        private val facings = "ESWN".repeat(2)
    }
}

class Ship2(var east: Int = 0, var north: Int = 0, val waypoint: Waypoint = Waypoint()) {
    fun performInstruction(instruction: String) {
        val action = instruction.first()
        val amount = instruction.drop(1).toInt()
        when (action) {
            in "NESW" -> waypoint.move(action, amount)
            'F' -> forward(amount)
            'R' -> waypoint.rotate(amount)
            'L' -> waypoint.rotate(360 - amount)
        }

    }

    fun forward(amount: Int) {
        east += amount * waypoint.east
        north += amount * waypoint.north
    }

    val manhattanDistance: Int
        get() = east.absoluteValue + north.absoluteValue
}

class Waypoint(var east: Int = 10, var north: Int = 1) {
    fun move(cardinalDirection: Char, amount: Int) {
        when (cardinalDirection) {
            'N' -> north += amount
            'E' -> east += amount
            'S' -> north -= amount
            'W' -> east -= amount
        }
        return
    }

    fun rotate(degrees: Int) {
        val turns = degrees / 90
        repeat(turns) {
            val newNorth = -east
            east = north
            north = newNorth
        }
    }
}