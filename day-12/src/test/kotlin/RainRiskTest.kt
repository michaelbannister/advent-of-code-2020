import org.junit.jupiter.api.Test
import strikt.api.Assertion
import strikt.api.expectThat
import strikt.assertions.isEqualTo

class RainRiskTest {
    val ship = Ship(0, 0, 'E')

    @Test
    fun `move north`() {
        ship.move("N20")
        expectThat(ship.position) isEqualTo Pair(0, 20)
    }

    @Test
    fun `move east`() {
        ship.move("E827")
        expectThat(ship.position) isEqualTo Pair(827, 0)
    }

    @Test
    fun `move south`() {
        ship.move("S30")
        expectThat(ship.position) isEqualTo Pair(0, -30)
    }

    @Test
    fun `move west`() {
        ship.move("W1")
        expectThat(ship.position) isEqualTo Pair(-1, 0)
    }

    @Test
    fun `move forward when facing east`() {
        ship.move("F10")
        expectThat(ship.position) isEqualTo Pair(10, 0)
    }

    @Test
    fun `turn right 90 degrees`() {
        ship.move("R90")
        expectThat(ship.facing) isEqualTo 'S'
    }

    @Test
    fun `turn right 180 degrees`() {
        ship.move("R180")
        expectThat(ship.facing) isEqualTo 'W'
    }

    @Test
    fun `turn left 90 degreese`() {
        ship.move("L90")
        expectThat(ship.facing) isEqualTo 'N'
    }
}
