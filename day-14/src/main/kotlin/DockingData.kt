fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val program = runV1Program(input)
    println(program.values.sum())

    val outputV2 = runV2Program(input)
    println(outputV2.values.sum())
}

fun runV1Program(input: String): Map<MemoryAddress, Long> {
    val memory = mutableMapOf<MemoryAddress, Long>()
    var mask: Transform = { it }
    input.lines().forEach { line ->
        if (line.startsWith("mask")) {
            mask = parseV1Mask(line)
        } else {
            val (address, value) = Regex("""mem\[(\d+)] = (\d+)""").matchEntire(line)!!.destructured
            memory[address.toLong()] = mask(value.toLong())
        }
    }
    return memory
}

fun runV2Program(input: String): Map<MemoryAddress, Long> {
    val memory = mutableMapOf<MemoryAddress, Long>()
    var mask: (MemoryAddress) -> List<MemoryAddress> = { listOf(it) }
    input.lines().forEach { line ->
        if (line.startsWith("mask")) {
            mask = parseV2Mask(line)
        } else {
            val (address, value) = Regex("""mem\[(\d+)] = (\d+)""").matchEntire(line)!!.destructured
            val addressesToWrite = mask(address.toLong())
            addressesToWrite.forEach { addr ->
                memory[addr] = value.toLong()
            }
        }
    }
    return memory
}

fun parseV1Mask(line: String): Transform {
    val mask = line.removePrefix("mask = ")
    val orMask: Long = mask.replace('X', '0').toLong(2)
    val andMask: Long = mask.replace('X', '1').toLong(2)
    return { value: Long -> value and andMask or orMask }
}

fun parseV2Mask(line: String): (MemoryAddress) -> List<MemoryAddress> {
    val mask = line.removePrefix("mask = ")

    return { originalAddress ->
        val adjustedAddress = originalAddress.toBitSequence().zip(mask).map { (orig, maskBit) ->
            when (maskBit) {
                '0' -> orig
                else -> maskBit
            }
        }.joinToString("")
        expandFloatingBits(adjustedAddress).map { it.toLong(2) }
    }
}

fun expandFloatingBits(bits: String): List<String> {
    return if ('X' !in bits) {
        listOf(bits)
    } else {
        expandFloatingBits(bits.replaceFirst('X', '1')) + expandFloatingBits(bits.replaceFirst('X', '0'))
    }
}

typealias MemoryAddress = Long
typealias Memory = MutableMap<MemoryAddress, Long>
typealias Transform = (Long) -> Long

fun MemoryAddress.toBitSequence() = this.toString(2).padStart(36, '0')