import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import strikt.api.expect
import strikt.api.expectThat
import strikt.assertions.containsExactlyInAnyOrder
import strikt.assertions.hasEntry
import strikt.assertions.isEqualTo

class DockingDataTest {

    @Nested
    inner class Version1 {
        val exampleProgram = """
            mask = XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X
            mem[8] = 11
            mem[7] = 101
            mem[8] = 0
        """.trimIndent()

        @Test
        fun `convert mask to bitwise ops`() {
            val mask = "XXXXXXXXXXXXXXXXXXXXXXXXXXXXX1XXXX0X"
            val orMask = mask.replace('X', '0').toLong(radix = 2)
            val andMask = mask.replace('X', '1').toLong(radix = 2)

            val applyMask = { value: Long -> value and andMask or orMask }
            expect {
                that(applyMask(11)) isEqualTo 73
                that(applyMask(101)) isEqualTo 101
                that(applyMask(0)) isEqualTo 64
            }
        }
    }

    @Nested
    inner class Version2 {
        @Test
        fun `parse mask`() {
            val mask = "000000000000000000000000000000X1001X"
            expectThat(parseV2Mask(mask)(42)).containsExactlyInAnyOrder(26L, 27L, 58L, 59L)
        }

        @Test
        fun `try example`() {
            val program = """
                mask = 000000000000000000000000000000X1001X
                mem[42] = 100
                mask = 00000000000000000000000000000000X0XX
                mem[26] = 1
            """.trimIndent()
            expectThat(runV2Program(program)).get { values.sum() } isEqualTo(208)
        }
    }
}