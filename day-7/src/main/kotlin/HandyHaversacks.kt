typealias Colour = String
typealias Quantity = Int

fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val allBags = parseRules(input)

    val count = allBags.values.count { it.canContain("shiny gold", allBags) }
    println("$count bag colours can contain at least one shiny gold bag")

    val goldBagSize = allBags["shiny gold"]!!.size(allBags)
    println("shiny gold bag contains $goldBagSize other bags")
}

fun parseRules(input: String): Map<Colour, Bag> = input.lines().map(::Bag).associateBy(Bag::colour)

class Bag(rule: String) {
    val colour: Colour
    val contents: Map<Colour, Quantity>
    init {
        val (colour, contentsText) = ruleStructure.matchEntire(rule)!!.destructured
        this.colour = colour
        contents = if (contentsText == "no other bags") {
            emptyMap()
        } else {
            contentsText.split(", ").map {
                val (quantity, otherColour) = quantityOfBag.matchEntire(it)!!.destructured
                otherColour to quantity.toInt()
            }.toMap()
        }
    }
    fun size(existingBags: Map<Colour, Bag>): Int {
        return if (contents.isEmpty()) 0
        else
            contents.asSequence().fold(0) { count, (colour, quantity) ->
                count + quantity * (1 + existingBags.getValue(colour).size(existingBags))
            }
    }

    fun canContain(colour: String, existingBags: Map<Colour, Bag>): Boolean =
        contents.containsKey(colour) ||
                contents.keys.any { c -> existingBags.getValue(c).canContain(colour, existingBags) }
}

private val ruleStructure = Regex("""(\w+ \w+) bags contain (.+).""")
private val quantityOfBag = Regex("""(\d+) (\w+ \w+) bags?""")
