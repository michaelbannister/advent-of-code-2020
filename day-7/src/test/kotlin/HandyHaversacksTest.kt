import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.hasEntry
import strikt.assertions.hasSize
import strikt.assertions.isEqualTo
import strikt.assertions.isFalse
import strikt.assertions.isTrue

class HandyHaversacksTest {
    val exampleRules = """
        light red bags contain 1 bright white bag, 2 muted yellow bags.
        dark orange bags contain 3 bright white bags, 4 muted yellow bags.
        bright white bags contain 1 shiny gold bag.
        muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.
        shiny gold bags contain 1 dark olive bag, 2 vibrant plum bags.
        dark olive bags contain 3 faded blue bags, 4 dotted black bags.
        vibrant plum bags contain 5 faded blue bags, 6 dotted black bags.
        faded blue bags contain no other bags.
        dotted black bags contain no other bags.
    """.trimIndent()

    val allBags: Map<Colour, Bag> = parseRules(exampleRules)

    @Test
    fun `parse terminal rule`() {
        expectThat(Bag("dotted black bags contain no other bags.")) {
            get { colour } isEqualTo "dotted black"
            get { contents } hasSize 0
        }
    }

    @Test
    fun `parse single-content rule`() {
        expectThat(Bag("bright white bags contain 1 shiny gold bag.")) {
            get { colour } isEqualTo "bright white"
            get { contents }.hasEntry("shiny gold", 1)
        }
    }

    @Test
    fun `parse multi-content rule`() {
        expectThat(Bag("muted yellow bags contain 2 shiny gold bags, 9 faded blue bags.")) {
            get { colour } isEqualTo "muted yellow"
            get { contents }.hasEntry("shiny gold", 2)
            get { contents }.hasEntry("faded blue", 9)
        }
    }

    @Test
    fun `check can contain for empty bag`() {
        val dottedBlack = allBags.getValue("dotted black")
        expectThat(dottedBlack.canContain("vibrant plum", allBags)).isFalse()
    }

    @Test
    fun `check bag cannot contain itself`() {
        val dottedBlack = allBags["dotted black"]!!
        expectThat(dottedBlack.canContain("dotted black", allBags)).isFalse()
    }

    @Test
    fun `check bag contains immediate content`() {
        val vibrantPlum = allBags["vibrant plum"]!!
        expectThat(vibrantPlum) {
            get { canContain("dotted black", allBags) }.isTrue()
            get { canContain("faded blue", allBags) }.isTrue()
        }
    }

    @Test
    fun `check bag contains nested content`() {
        val lightRed = allBags["light red"]!!
        expectThat(lightRed) {
            get { canContain("shiny gold", allBags) }.isTrue()
            get { canContain("faded blue", allBags) }.isTrue()
            get { canContain("dark orange", allBags) }.isFalse()
        }
    }

    @Test
    fun `check part 1 example problem`() {
        val count = allBags.values.count { it.canContain("shiny gold", allBags) }
        expectThat(count) isEqualTo 4
    }

    @Test
    fun `check empty bag has 0 contained bags`() {
        val fadedBlue = allBags["faded blue"]!!
        expectThat(fadedBlue) {
            get { size(allBags) } isEqualTo 0
        }
    }

    @Test
    fun `check size one level deep`() {
        val vibrantPlum = allBags["vibrant plum"]!!
        expectThat(vibrantPlum) {
            get { size(allBags) } isEqualTo 11
        }
    }

    @Test
    fun `check size two levels deep`() {
        val shinyGold = allBags["shiny gold"]!!
        expectThat(shinyGold) {
            get { size(allBags) } isEqualTo 32 // 3 + 1*7 + 2*11
        }
    }
}