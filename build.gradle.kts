plugins {
    kotlin("jvm") version "1.4.10"
}

allprojects {
    plugins.apply("kotlin")
    repositories {
        jcenter()
    }
}
subprojects {
    val junitVersion = "5.7.0"
    val striktVersion = "0.28.0"

    dependencies {
        testImplementation("org.junit.jupiter:junit-jupiter-api:$junitVersion")
        testImplementation("org.junit.jupiter:junit-jupiter-params:$junitVersion")
        testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:$junitVersion")

        testImplementation("io.strikt:strikt-core:$striktVersion")
    }

    tasks.withType<org.jetbrains.kotlin.gradle.tasks.KotlinCompile> {
        kotlinOptions {
            jvmTarget = "11"
        }
    }
    tasks.named<Test>("test") {
        useJUnitPlatform()
    }
//    application {
//        mainClassName = "org.bannisters.AppKt"
//    }
}
