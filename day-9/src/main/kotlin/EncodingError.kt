fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val numbers = input.lines().map(String::toLong)
    val invalidNumber = findFirstInvalidXmasNumber(numbers, 25)
    println("Invalid XMAS number: $invalidNumber")

    val range = findContiguousRangeSummingTo(invalidNumber, numbers)
    val answer = range.maxOrNull()!! + range.minOrNull()!!
    println("found range ${range.joinToString()}. Sum of min & max is $answer")
}

fun findFirstInvalidXmasNumber(numbers: List<Long>, preambleLength: Int): Long {
    val preamble = ArrayDeque(numbers.subList(0, preambleLength))
    val rest = numbers.drop(preambleLength)

    return findFirstInvalidXmasNumber(preamble, rest)
}

fun findFirstInvalidXmasNumber(preamble: ArrayDeque<Long>, rest: List<Long>): Long {
    val target = rest.first()
    val part = preamble.find {
        val complement = target - it
        it != complement && complement in preamble
    }
    return if (part == null)
        target
    else {
        preamble.apply {
            removeFirst()
            addLast(target)
        }
        findFirstInvalidXmasNumber(preamble, rest.drop(1))
    }
}

fun findContiguousRangeSummingTo(target: Long, numbers: List<Long>): List<Long> {
    numbers.forEachIndexed { index, first ->
        if (first != target) {
            var acc: Long = 0
            val range = numbers.subList(index, numbers.size).takeWhile {
                acc += it
                acc < target
            }
            if (acc == target)
                return range
        }
    }
    throw RuntimeException("No solution found :…(")
}

