import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo

class EncodingErrorTest {
    val example = """
        35
        20
        15
        25
        47
        40
        62
        55
        65
        95
        102
        117
        150
        182
        127
        219
        299
        277
        309
        576
    """.trimIndent()

    @Test
    fun `find first invalid number`() {
        val numbers = example.lines().map(String::toLong)
        val invalid = findFirstInvalidXmasNumber(numbers, 5)
        expectThat(invalid) isEqualTo 127
    }
}