import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isFalse
import strikt.assertions.isTrue

class TobogganTrajectoryTest {

    @Test
    fun `should identify open space`() {
        expectThat(exampleMap.isTree(1, 3)).isFalse()
    }

    @Test
    fun `should identify tree`() {
        expectThat(exampleMap.isTree(2, 6)).isTrue()
    }

    @Test
    fun `should wrap around`() {
        expectThat(exampleMap.isTree(4, 12)).isTrue()
    }

    @Test
    fun `should count trees on traversal`() {
        val progress = exampleMap.traverse(Slope(down = 1, right = 3))
        expectThat(progress).get { trees } isEqualTo 7
    }

    @Test
    fun `check part two against example`() {
        val slopesToTest = listOf(
            Slope(right = 1, down = 1),
            Slope(right = 3, down = 1),
            Slope(right = 5, down = 1),
            Slope(right = 7, down = 1),
            Slope(right = 1, down = 2)
        )
        val result = slopesToTest.map(exampleMap::traverse).map(Progress::trees).fold(1, Int::times)
        expectThat(result) isEqualTo(336)
    }

    val exampleMapInput = """
        ..##.......
        #...#...#..
        .#....#..#.
        ..#.#...#.#
        .#...##..#.
        ..#.##.....
        .#.#.#....#
        .#........#
        #.##...#...
        #...##....#
        .#..#...#.#
    """.trimIndent()
    val exampleMap = Map(exampleMapInput.lines())
}