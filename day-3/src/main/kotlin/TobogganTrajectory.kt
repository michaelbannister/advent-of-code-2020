fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val map = Map(input.lines())

    val slopesToTest = listOf(
        Slope(right = 1, down = 1),
        Slope(right = 3, down = 1),
        Slope(right = 5, down = 1),
        Slope(right = 7, down = 1),
        Slope(right = 1, down = 2)
    )
    val product = slopesToTest.map { slope ->
        val trees = map.traverse(slope).trees
        println("$slope: hit $trees trees")
        trees
    }.fold(1, Long::times)

    println("Product of trees: $product")
}


class Map(private val mapText: List<String>) {
    val width = mapText.first().length
    val height = mapText.size

    fun isTree(down: Int, right: Int): Boolean =
        mapText[down][right % width] == '#'

    fun traverse(slope: Slope): Progress {
        var progress = Progress.INITIAL
        while (progress.position.down < height) {
            val isTree = progress.position.let { isTree(it.down, it.right) }
            progress = Progress(progress.trees + isTree.toInt(), progress.position.move(slope))
        }
        return progress
    }
}

data class Slope(val down: Int, val right: Int)
data class Position(val down: Int, val right: Int) {
    companion object {
        val START = Position(0, 0)
    }

    fun move(slope: Slope) = copy(down + slope.down, right + slope.right)
}

data class Progress(val trees: Int, val position: Position) {
    companion object {
        val INITIAL = Progress(0, Position.START)
    }
}

fun Boolean.toInt() = if (this) 1 else 0