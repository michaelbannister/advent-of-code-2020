import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import strikt.api.expect
import strikt.api.expectThat
import strikt.assertions.isEqualTo
import strikt.assertions.isFalse
import strikt.assertions.isTrue

class PasswordPhilosophyTest {
    @Test
    fun `test simple sample`() {
        val input = """
            1-3 a: abcde
            1-3 b: cdefg
            2-9 c: ccccccccc
        """.trimIndent()

        expectThat(howManyPasswordsValid(input.lines(), sledRentalPolicyReader)) isEqualTo 2
    }

    @Test
    fun `should parse password`() {
        val (_, password) = parsePolicyAndPassword("1-3 a: abcde", sledRentalPolicyReader)
        expectThat(password) isEqualTo "abcde"
    }

    @Test
    fun `should parse policy`() {
        val (policy, _) = parsePolicyAndPassword("1-3 a: abc", sledRentalPolicyReader)
        expect {
            that(policy("bcd")).isFalse()
            that(policy("abcd")).isTrue()
            that(policy("abcadeaf")).isTrue()
            that(policy("bcaadeaa")).isFalse()
        }
    }

    @Nested
    inner class OfficialTobogganCorporatePolicy {
        val policy = officialTobogganCorporatePolicy("1-3 b")
        @Test
        fun `should consider password invalid if neither position contains required letter`() {
            expectThat(policy("cbdbb")).isFalse()
        }

        @Test
        fun `should consider password valid if only first position contains required letter`() {
            expectThat(policy("bbdbb")).isTrue()
        }

        @Test
        fun `should consider password valid if only second position contains required letter`() {
            expectThat(policy("abbbb")).isTrue()
        }

        @Test
        fun `should consider password invalid if both positions contain required letter`() {
            expectThat(policy("bbbbb")).isFalse()
        }
    }
}