typealias Policy = (String) -> Boolean
typealias PolicyReader = (String) -> Policy

fun howManyPasswordsValid(input: Iterable<String>, policyReader: PolicyReader): Int {
    return input.map { parsePolicyAndPassword(it, policyReader) }.count { (policy, password) -> policy(password) }
}

fun parsePolicyAndPassword(input: String, policyReader: PolicyReader): Pair<Policy, String> {
    val (policyDescription, password) = input.split(": ", limit = 2)
    val policy = policyReader(policyDescription)
    return Pair(policy, password)
}

val sledRentalPolicyReader: PolicyReader = { policyDescription ->
    val (countRange, letter) = policyDescription.split(' ', limit = 2)
    val (rangeStart, rangeEnd) = countRange.split('-', limit = 2).map(String::toInt)

    val predicate = { candidate: String ->
        candidate.count { it == letter.first() } in rangeStart..rangeEnd
    }
    predicate
}

val officialTobogganCorporatePolicy: PolicyReader = { policyDescription ->
    val (countRange, letter) = policyDescription.split(' ', limit = 2)
    val (firstPosition, secondPosition) = countRange.split('-', limit = 2).map(String::toInt).map(Int::dec)

    val requiredCharacter = letter.first()

    val predicate = { candidate: String ->
        (candidate[firstPosition] == requiredCharacter) xor (candidate[secondPosition] == requiredCharacter)
    }
    predicate
}

fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    println("sled rental policy interpretation")
    val validPasswordCount = howManyPasswordsValid(input.lines(), sledRentalPolicyReader)
    println(validPasswordCount)
    print("\n\n")

    println("Official Toboggan Corporate Policy")
    val actuallyValidPasswordCount = howManyPasswordsValid(input.lines(), officialTobogganCorporatePolicy)
    println(actuallyValidPasswordCount)
}
