fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val dimension = PocketDimension3D.parseInitialState(input)
    dimension.boot()
    println("active points: ${dimension.activePoints.size}")
}

class PocketDimension3D(initialActivePoints: Set<Point3D>) {
    var activePoints = initialActivePoints

    fun cycle() {
        val inactivePointsWithAtLeastOneActiveNeighbour: Set<Point3D> =
            activePoints.flatMap(Point3D::neighbours).toSet() - activePoints
        val inactivePointsBecomingActive: Set<Point3D> =
            inactivePointsWithAtLeastOneActiveNeighbour.filter { it.countActiveNeighbours() == 3 }.toSet()
        val activePointsRemainingActive = activePoints.filter { it.countActiveNeighbours() in 2..3 }.toSet()
        activePoints = activePointsRemainingActive + inactivePointsBecomingActive
    }

    fun boot() = repeat(6) { cycle() }

    fun Point3D.isActive() = this in activePoints
    fun Point3D.countActiveNeighbours() = this.neighbours().count { it.isActive() }

    companion object {
        fun parseInitialState(input: String): PocketDimension3D =
            input.lines().flatMapIndexed { y, line ->
                line.mapIndexedNotNull { x, char ->
                    if (char == '#') Point3D(x, y, 0) else null
                }
            }.toSet().let(::PocketDimension3D)
    }
}

data class Point3D(val x: Int, val y: Int, val z: Int)

fun Point3D.neighbours(): List<Point3D> =
    listOf(x - 1, x, x + 1).flatMap { nx ->
        listOf(y - 1, y, y + 1).flatMap { ny ->
            listOf(z - 1, z, z + 1).mapNotNull { nz ->
                if (nx == x && ny == y && nz == z) {
                    null
                } else {
                    Point3D(nx, ny, nz)
                }
            }
        }
    }