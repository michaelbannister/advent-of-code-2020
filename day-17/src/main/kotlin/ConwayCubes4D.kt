fun main() {
    val input = object {}::class.java.getResource("input.txt")!!.readText()
    val dimension = PocketDimension4D.parseInitialState(input)
    dimension.boot()
    println("active points: ${dimension.activePoints.size}")
}

class PocketDimension4D(initialActivePoints: Set<Point4D>) {
    var activePoints = initialActivePoints

    fun cycle() {
        val inactivePointsWithAtLeastOneActiveNeighbour: Set<Point4D> =
            activePoints.flatMap(Point4D::neighbours).toSet() - activePoints
        val inactivePointsBecomingActive: Set<Point4D> =
            inactivePointsWithAtLeastOneActiveNeighbour.filter { it.countActiveNeighbours() == 3 }.toSet()
        val activePointsRemainingActive = activePoints.filter { it.countActiveNeighbours() in 2..3 }.toSet()
        activePoints = activePointsRemainingActive + inactivePointsBecomingActive
    }

    fun boot() = repeat(6) { cycle() }

    fun Point4D.isActive() = this in activePoints
    fun Point4D.countActiveNeighbours() = this.neighbours().count { it.isActive() }

    companion object {
        fun parseInitialState(input: String): PocketDimension4D =
            input.lines().flatMapIndexed { y, line ->
                line.mapIndexedNotNull { x, char ->
                    if (char == '#') Point4D(0, x, y, 0) else null
                }
            }.toSet().let(::PocketDimension4D)
    }
}

data class Point4D(val w: Int, val x: Int, val y: Int, val z: Int)

fun Point4D.neighbours(): List<Point4D> =
    listOf(w - 1, w, w + 1).flatMap { nw ->
        listOf(x - 1, x, x + 1).flatMap { nx ->
            listOf(y - 1, y, y + 1).flatMap { ny ->
                listOf(z - 1, z, z + 1).mapNotNull { nz ->
                    if (nw ==w && nx == x && ny == y && nz == z) {
                        null
                    } else {
                        Point4D(nw, nx, ny, nz)
                    }
                }
            }
        }
    }
