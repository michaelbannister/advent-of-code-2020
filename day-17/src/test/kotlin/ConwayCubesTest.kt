import org.junit.jupiter.api.Test
import strikt.api.expectThat
import strikt.assertions.hasSize

class ConwayCubesTest {

    @Test
    fun `example`() {
        val dimension = PocketDimension3D(setOf(
            Point3D(1,0,0),
            Point3D(2,1,0),
            Point3D(0, 2, 0),
            Point3D(1, 2, 0),
            Point3D(2, 2, 0),
        ))

        repeat(6) {
            dimension.cycle()
        }

        expectThat(dimension.activePoints).hasSize(112)
    }
}